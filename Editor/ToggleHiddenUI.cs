﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.UI;

public class ToggleHiddenUI : MonoBehaviour
{
	
	[MenuItem("Tools/Show Hidden UI")]
	public static void ShowHiddenUI()
	{
		foreach(AnimateUI ui in FindObjectsOfType<AnimateUI>())
		{
			if (!ui.startOpen && ui.animationType != AnimateUI.AnimType.None && ui.animationType != AnimateUI.AnimType.AlphaFade)
				ui.transform.localScale = Vector3.one;
		}
	}

	[MenuItem("Tools/Hide Hidden UI")]
	public static void HideHiddenUI()
	{
		foreach (AnimateUI ui in FindObjectsOfType<AnimateUI>())
		{
			if (!ui.startOpen && ui.animationType != AnimateUI.AnimType.None && ui.animationType != AnimateUI.AnimType.AlphaFade)
				ui.transform.localScale = Vector3.zero;

			if (!ui.startOpen && ui.animationType == AnimateUI.AnimType.AlphaFade)
			{
				Color c = ui.GetComponent<Image>().color;
				if (ui.alphaFadeAlpha == -1) ui.alphaFadeAlpha = c.a;
				c.a = 0;
				ui.GetComponent<Image>().color = c;
			}
		}
	}

	[MenuItem("Tools/Focus Selected UI %#q")]
	public static void FocusSelectedUI()
	{
		foreach (AnimateUI ui in FindObjectsOfType<AnimateUI>())
		{
			if (!ui.startOpen && ui.animationType != AnimateUI.AnimType.None && ui.animationType != AnimateUI.AnimType.AlphaFade)
			{
				ui.transform.localScale = Vector3.one * (Selection.activeGameObject.GetComponent<AnimateUI>() == ui ? 1 : 0);
			}

			if (!ui.startOpen && ui.animationType == AnimateUI.AnimType.AlphaFade)
			{
				Color c = ui.GetComponent<Image>().color;
				if (ui.alphaFadeAlpha == -1) ui.alphaFadeAlpha = c.a;

				if (Selection.activeGameObject.GetComponent<AnimateUI>() == ui || Selection.activeGameObject.GetComponent<AnimateUI>()?.Parent == ui)
				{
					c.a = ui.alphaFadeAlpha;
					ui.GetComponent<Image>().color = c;
				}
				else
				{
					c.a = 0;
					ui.GetComponent<Image>().color = c;
				}
			}
		}
	}
}
