﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimateFloat
{

	float current, target, animLength;

	float animSpeed;

	float lastGetTime;

	bool gameTime;
	bool animating = false;

	public AnimateFloat(float val, float animTime = 1, bool isGameTime = false)
	{
		gameTime = isGameTime;
		lastGetTime = (gameTime ? Time.time : Time.realtimeSinceStartup);

		current = target = val;

		animLength = animTime;
		animating = false;
	}

	void Normalize()
	{
		float dt = (gameTime ? Time.time : Time.realtimeSinceStartup) - lastGetTime;
		float left = (target - current) / animSpeed;

		if(left > dt)
		{
			current += dt * animSpeed;
		}
		else
		{
			animating = false;
			current = target;
		}

		lastGetTime = (gameTime ? Time.time : Time.realtimeSinceStartup);
	}

	public void SetInstant(float newValue)
	{
			Normalize();
			target = newValue;
			animSpeed = 0;
			current = newValue;
}


	public void Set(float newValue, bool overrideSame = false, bool resetAnimTime = true)
	{
		Normalize();

		if(Math.Abs(target - newValue) > .01 || overrideSame)
		{
			if(resetAnimTime)
			{
				animSpeed = (newValue - current) / animLength;
			}
			else
			{
				float left = animLength - (target - current) / animSpeed;
				animSpeed = (newValue - current) / left;
			}
			target = newValue;
			animating = true;
		}
	}

	public void Set(double newValue, bool overrideSame = false, bool resetAnimTime = true) => Set((float)newValue, overrideSame, resetAnimTime);

	public float Current()
	{
		Normalize();
		return current;
	}

	public float Target()
	{
		Normalize();
		return target;
	}

	public static implicit operator float(AnimateFloat f)
	{
		if (f == null) return 0;
		return f.Current();
	}
}
