﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Linq;

public static class RandomExtended
{
	public static double Range(double min, double max)
	{
		System.Random random = new System.Random(Random.state.GetHashCode());
		int a = Random.Range(0, 10); // To change the state

		return random.NextDouble() * (max - min) + min;
	}

	public static float Range(float min, float max) => Random.Range(min, max);

	public static int Range(int min, int max) => Random.Range(min, max);
}

public static class MathD
{
	public static double Clamp(double val, double min, double max) => System.Math.Min(System.Math.Max(val, min), max);

	public static double Clamp01(double val) => Clamp(val, 0, 1);
}

public static class Extensions
{
	/// <summary>
	/// Returns element at index given, or last or first if index out of range
	/// </summary>
	public static T AtCapped<T>(this IList<T> list, int index)
	{
		return list[Mathf.Clamp(index, 0, list.Count - 1)];
	}

	/// <summary>
	/// Returns first element that maximises the function given, or default for empty enumerable.
	/// </summary>
	public static T Maximise<T>(this IEnumerable<T> container, System.Func<T, float> fun)
	{
		T bestElement = default;
		float bestValue = float.MinValue;

		foreach (T element in container)
		{
			float val = fun(element);
			if (val > bestValue)
			{
				bestValue = val;
				bestElement = element;
			}
		}

		return bestElement;
	}

	/// <summary>
	/// Returns maximum of the function given on IEnumerable, or default V for empty enumerable.
	/// </summary>
	public static V MaxValue<T, V>(this IEnumerable<T> container, System.Func<T, V> fun) where V : System.IComparable
	{
		bool any = false;
		V bestValue = default;

		foreach (T element in container)
		{

			V val = fun(element);
			if (!any || val.CompareTo(bestValue) > 0)
			{
				any = true;
				bestValue = val;
			}
		}

		return bestValue;
	}


	/// <summary>
	/// Applies f to accumulator and all elements of container in order and returns the result.
	/// </summary>
	public static T LFold<T,LT>(this IEnumerable<LT> container, T accumulator, System.Func<T, LT, T> f)
	{
		foreach (LT element in container) accumulator = f(accumulator, element);
		return accumulator;
	}

	/// <summary>
	/// Returns first element that minimises the function given, or default for empty enumerable.
	/// </summary>
	public static T Minimise<T>(this IEnumerable<T> container, System.Func<T, float> fun)
	{
		T bestElement = default;
		float bestValue = float.MaxValue;

		foreach (T element in container)
		{
			float val = fun(element);
			if (val < bestValue)
			{
				bestValue = val;
				bestElement = element;
			}
		}

		return bestElement;
	}

	public static void ForEach<T>(this IEnumerable<T> container, System.Action<T, int> action)
	{
		int index = 0;
		foreach(T element in container)
		{
			action(element, index);
			index++;
		}
	}

	/// <summary>
	/// Finds object with this name in transform tree. No slashes.
	/// </summary>
	public static Transform FindRecursive(this Transform t, string n)
	{
		if (t.gameObject.name.Equals(n)) return t;
		
		foreach(Transform child in t)
		{
			Transform found = FindRecursive(child, n);
			if (found != null) return found;
		}

		return null;
	}
}

public class Utils : MonoBehaviour
{

	#region Timeouts
	[System.Serializable]
	public struct TimeoutCallback : System.IComparable
	{
		internal float T;

		internal bool isReoccuring;
		internal float reoccurTime;

		internal TimeoutCallback(float T)
		{
			this.T = T;
			this.isReoccuring = false;
			this.reoccurTime = 0;
		}

		internal TimeoutCallback(float T, float loop)
		{
			this.T = T;
			this.isReoccuring = true;
			this.reoccurTime = loop;
		}

		public static TimeoutCallback None => new TimeoutCallback(-1);

		public override bool Equals(object obj)
		{
			if (obj.GetType() != typeof(TimeoutCallback)) return false;
			return ((TimeoutCallback)obj).T == T;
		}

		public override int GetHashCode()
		{
			return T.GetHashCode();
		}

		public int CompareTo(object obj)
		{
			if (obj.GetType() != typeof(TimeoutCallback)) return 0;
			return T.CompareTo(((TimeoutCallback)obj).T);
		}

		public static bool operator !=(TimeoutCallback t1, TimeoutCallback t2)
		{
			return t1.T != t2.T;
		}

		public static bool operator ==(TimeoutCallback t1, TimeoutCallback t2)
		{
			return t1.T == t2.T;
		}
	}

	static SortedList<TimeoutCallback, System.Delegate> toExecute = new SortedList<TimeoutCallback, System.Delegate>();

	public static void ExecuteInstant(TimeoutCallback identifier)
	{
		if (toExecute.ContainsKey(identifier))
		{
			toExecute[identifier].DynamicInvoke();
			toExecute.Remove(identifier);
		}
	}

	public static void RemoveTimeout(TimeoutCallback identifier)
	{
		if (toExecute.ContainsKey(identifier))
		{
			toExecute.Remove(identifier);
		}
	}

	public delegate void Fun();

	void HandleTimeouts()
	{
		while (toExecute.Count > 0 && toExecute.First().Key.T <= Time.time)
		{
			var front = toExecute.First();
			toExecute.RemoveAt(0);

			front.Value.DynamicInvoke();

			if (front.Key.isReoccuring)
			{
				SetTimeout(front.Value, front.Key.reoccurTime, true);
			}
		}
	}

	public static TimeoutCallback SetTimeout(System.Delegate func, float after, bool reoccuring = false)
	{
		float T = 0 + after;

		try { T = Time.time + after; }
		catch (UnityException ignore) { /*cannot getTime now*/ }

		while (toExecute.ContainsKey(new TimeoutCallback(T)))
		{
			T += T / 10000000 + 0.0001f;
		}

		TimeoutCallback callback = reoccuring ? new TimeoutCallback(T, after) : new TimeoutCallback(T);

		toExecute.Add(callback, func);
		return callback;
	}

	#endregion

	public static System.Predicate<T> None<T>() => e => false;
	public static System.Predicate<T> All<T>() => e => true;

	public static Camera mainCamera;

	static string[] Suffix = { "", "K", "M", "B", "T", "q", "Q", "s", "S", "O", "N", "d", "U", "D"};

	private void Awake()
	{
		mainCamera = Camera.main;
	}


	public static float Binearch(System.Func<float, float> func, float target, float start = -1000, float end = -1000, float epsilon = 0.0001f)
	{
		bool increasing = func(start) < func(end);

		while(end - start > epsilon)
		{
			float mid = (start + end) / 2;
			float output = func(mid);

			if (output > target ^ increasing) start = mid;
			else end = mid;
		}

		return (end + start) / 2;
	}

	public static double Binearch(System.Func<double, double> func, double target, double start = -1000, double end = -1000, double epsilon = 0.0001f)
	{
		bool increasing = func(start) < func(end);

		while (end - start > epsilon)
		{
			double mid = (start + end) / 2;
			double output = func(mid);

			if (output > target ^ increasing) start = mid;
			else end = mid;
		}

		return (end + start) / 2;
	}



	public static string FormatAmount(double amount, int maxPlaces, bool roundInt = true)
	{
		if (roundInt) amount = System.Math.Floor(amount);

		//TODO
		int a = 0;
		while (amount >= 1000)
		{
			amount /= 1000;
			a++;
		}
		
		if (amount >= 100) maxPlaces--;
		if (amount >= 10) maxPlaces--;
		maxPlaces--;
		string s = amount.ToString("F" + Mathf.Max(0, maxPlaces));
		if (s.Contains('.') || s.Contains(','))
		{
			s = s.TrimEnd('0');
			s = s.TrimEnd('.', ',');
		}
		return s + Suffix[a];
	}

	public static string FormatAmount(float amount, int maxPlaces, bool roundInt = true) =>
		FormatAmount((double)amount, maxPlaces, roundInt); 

	public static Vector3 RandomInCircle()
	{
		return Quaternion.Euler(0, Random.Range(0, 360f), 0) * Vector3.forward * Mathf.Sqrt(Random.Range(0, 1f));
	}

	public static Vector3 RandomInCircleXY()
	{
		return Quaternion.Euler(Random.Range(0, 360f), 0, 0) * Vector3.up * Mathf.Sqrt(Random.Range(0, 1f));
	}

	public static float AspectRatio()
	{
		return Screen.width / (float)Screen.height;
	}

	public static Vector3 Flatten(Vector3 v, float newY = 0)
	{
		v.y = newY;
		return v;
	}

	public static Vector3 FlattenZ(Vector3 v, float newZ = 0)
	{
		v.z = newZ;
		return v;
	}

	public static T Closest<T>(Vector3 from, System.Predicate<T> predicate) where T:MonoBehaviour
	{
		T selected = null;
		foreach(T obj in FindObjectsOfType<T>())
		{
			if (selected == null || (selected.transform.position - from).magnitude > (obj.transform.position - from).magnitude)
				if(predicate(obj)) selected = obj;
		}
		return selected;
	}

	public static T HighestFit<T>( System.Func<T, float> valueFunction, System.Predicate<T> predicate) where T : MonoBehaviour
	{
		T selected = null;
		float bestValue = float.MinValue;
		foreach (T obj in FindObjectsOfType<T>())
		{
			if (!predicate(obj)) continue;

			float result = valueFunction(obj);

			if (selected == null || result > bestValue)
			{
				bestValue = result;
				selected = obj;
			}
		}
		return selected;
	}

	public static T LowestFit<T>(System.Func<T, float> valueFunction, System.Predicate<T> predicate) where T : MonoBehaviour
	{
		T selected = null;
		float bestValue = float.MaxValue;
		foreach (T obj in FindObjectsOfType<T>())
		{
			if (!predicate(obj)) continue;

			float result = valueFunction(obj);

			if (selected == null || result < bestValue)
			{
				bestValue = result;
				selected = obj;
			}
		}
		return selected;
	}

	public static void DrawString(string text, Vector3 worldPos, Color? colour = null)
	{
#if UNITY_EDITOR
		UnityEditor.Handles.BeginGUI();
		if (colour.HasValue) GUI.color = colour.Value;
		var view = UnityEditor.SceneView.currentDrawingSceneView;
		Vector3 screenPos = view.camera.WorldToScreenPoint(worldPos);
		if (screenPos.z > 0)
		{
			Vector2 size = GUI.skin.label.CalcSize(new GUIContent(text));
			GUI.Label(new Rect(screenPos.x - (size.x / 2), -screenPos.y + view.position.height - size.y / 2, size.x, size.y), text);
		}
		UnityEditor.Handles.EndGUI();
#endif
	}

	public static void SimulatePhysics(float t = 10, float dt = 0.5f, bool removeVelocities = true)
	{
		Physics.autoSimulation = false;
		for(float f = 0; f < t; f += dt)
		{
			Physics.Simulate(dt);
		}
		Physics.autoSimulation = true;

		if (removeVelocities) foreach (Rigidbody r in FindObjectsOfType<Rigidbody>()) r.velocity = Vector3.zero;
	}

	public static float Length(LineRenderer line)
	{

		float o = 0;
		for (int i = 0; i < line.positionCount - 1; i++)
		{
			o += (line.GetPosition(i) - line.GetPosition(i + 1)).magnitude;
		}

		if (line.loop) o += (line.GetPosition(0) - line.GetPosition(line.positionCount - 1)).magnitude;
		//Debug.Log("Line " + line.name + " measueres" + o + " (" + line.positionCount + " segments)");
		return o;
	}

	public static Vector3 NormPointOnLine(LineRenderer line, float normalisedDistance)
	{
		float len = Length(line);
		return PointOnLine(line, normalisedDistance * len);
	}

	public static Vector3 PointOnLine(LineRenderer line, float distance)
	{
		for (int i = 0; i < line.positionCount - 1; i++)
		{
			if ((line.GetPosition(i + 1) - line.GetPosition(i)).magnitude < distance)
				distance -= (line.GetPosition(i + 1) - line.GetPosition(i)).magnitude;

			else
			{
				//Debug.Log("Segment " + i);
				if (line.useWorldSpace)
					return Vector3.MoveTowards(line.GetPosition(i), line.GetPosition(i + 1), distance);
				return line.transform.TransformPoint(Vector3.MoveTowards(line.GetPosition(i), line.GetPosition(i + 1), distance));
			}
		}

		if (line.loop)
		{
			Debug.Log("Not implemented");
		}

		Debug.Log("Requestet point further than line length");
		if (line.useWorldSpace)
			return line.GetPosition(line.positionCount - 1);
		return line.transform.TransformPoint(line.GetPosition(line.positionCount - 1));
	}

	public static void DrawCircle(Vector3 center, float radius, int quality = 32)
	{
		float seg = 2 * Mathf.PI / quality;
		for (int i = 0; i < quality; i++)
		{
			Gizmos.DrawLine(center + radius * new Vector3(Mathf.Cos(i * seg), 0, Mathf.Sin(i * seg)), center + radius * new Vector3(Mathf.Cos((i + 1) * seg), 0, Mathf.Sin((i + 1) * seg)));
		}
	}

	public static void DrawArrow(Vector3 from, Vector3 to, float arrowscale = 0.1f, bool segmented = false, float segmentLength = 0.1f)
	{
		if (segmented) DrawSegmentedLine(from, to, segmentLength);
		else Gizmos.DrawLine(from, to);
		Vector3 dir = (to - from).normalized;

		if (dir != Vector3.zero)
		{
			Vector3 left = Quaternion.LookRotation(dir) * Vector3.left;
			Gizmos.DrawLine(to - dir * arrowscale, to - dir * arrowscale * 2 + left * arrowscale);
			Gizmos.DrawLine(to - dir * arrowscale, to - dir * arrowscale * 2 - left * arrowscale);
		}
	}

	public static void DrawSegmentedLine(Vector3 from, Vector3 to, float segmentLength = 0.1f)
	{
		Vector3 dir = to - from;
		float length = dir.magnitude;
		dir = dir.normalized;
		Vector3 pos = from;
		bool draw = true;
		while (length > 0)
		{
			segmentLength = Mathf.Min(segmentLength, length);
			if (draw) Gizmos.DrawLine(pos, pos + dir * segmentLength);
			pos += dir * segmentLength;
			length -= segmentLength;
			draw = !draw;
		}
	}

	public static void DestroyChildren(GameObject g, bool immediate = false)
	{
		DestroyChildren(g.transform, immediate);
	}

	public static void DestroyChildren(Transform t, bool immediate = false)
	{
		foreach (Transform child in t)
		{
			if (immediate) DestroyImmediate(child.gameObject);
			else Destroy(child.gameObject);
		}
	}

	public static Bounds GetBounds(GameObject g)
	{
		Bounds bounds = new Bounds(g.transform.position, Vector3.zero);
		foreach (MeshRenderer renderer in g.GetComponentsInChildren<MeshRenderer>())
		{
			Bounds b = renderer.bounds;
			bounds.Encapsulate(b.min);
			bounds.Encapsulate(b.max);
		}

		return bounds;
	}

	public static Vector3 RandomInCircle(int seed)
	{
		float A = seed % 360;
		float B = seed % 41;
		return Quaternion.Euler(0, A, 0) * new Vector3(B / 41, 0);
	}

	public static T RandomElement<T>(List<T> list)
	{
		return list[Random.Range(0, list.Count)];
	}

	public static T RandomElement<T>(T[] array)
	{
		return array[Random.Range(0, array.Length)];
	}

	public static bool IsSeen(GameObject g, float vScale = 1)
	{
		if (g)
			return IsSeen(g.transform.position, vScale);
		else
			return false;
	}

	public static bool IsSeen(Vector3 position, float vScale = 1)
	{
		Vector3 v = mainCamera.WorldToViewportPoint(position) / vScale;
		return (v.x >= 0 && v.x <= 1 && v.y >= 0 && v.y <= 1);

	}

	public static void MoveRectTransform(RectTransform t, Vector2 pos)
	{
		Vector2 anchorDiff = t.anchorMax - t.anchorMin;
	}

	public static bool CurveTransform(Transform what, Transform from, Transform to, float place)
	{
		Vector3 v1 = from.rotation * Vector3.forward;
		Vector3 v2 = to.rotation * Vector3.forward;

		if (TransformForwardIntersection(out Vector3 vp, from, to))
		{
			var position = @from.position;
			v1 = position + place * (vp - position);
			v2 = vp + place * (to.position - vp);

			what.transform.rotation = Quaternion.LookRotation(v2 - v1);
			what.transform.position = v1 + place * (v2 - v1);


			return true;
		}
		else return false;
	}

	public static bool TransformForwardIntersection(out Vector3 intersection, Transform t1, Transform t2)
	{
		Vector3 v1 = t1.rotation * Vector3.forward;
		Vector3 v2 = t2.rotation * Vector3.forward;

		return LineLineIntersection(out intersection, t1.transform.position, v1, t2.transform.position, v2);
	}

	public static bool IsAnyInputOverUI()
	{
		if (UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject()) return true;

		for (int i = 0; i < Input.touchCount; i++)
		{
			if (UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject(Input.GetTouch(i).fingerId)) return true;
		}
		return false;
	}


	//Calculate the intersection point of two lines. Returns true if lines intersect, otherwise false.
	//Note that in 3d, two lines do not intersect most of the time. So if the two lines are not in the 
	//same plane, use ClosestPointsOnTwoLines() instead.
	public static bool LineLineIntersection(out Vector3 intersection, Vector3 linePoint1, Vector3 lineVec1, Vector3 linePoint2, Vector3 lineVec2)
	{

		//TODO to nie działa naprawde ale tu naprawi xD
		lineVec1 = Flatten(lineVec1.normalized);
		lineVec2 = Flatten(lineVec2.normalized);


		Vector3 lineVec3 = linePoint2 - linePoint1;
		Vector3 crossVec1and2 = Vector3.Cross(lineVec1, lineVec2);
		Vector3 crossVec3and2 = Vector3.Cross(lineVec3, lineVec2);

		float planarFactor = Vector3.Dot(lineVec3, crossVec1and2);

		//is coplanar, and not parrallel
		if (Mathf.Abs(planarFactor) < 0.0001f && crossVec1and2.sqrMagnitude > 0.0001f)
		{
			float s = Vector3.Dot(crossVec3and2, crossVec1and2) / crossVec1and2.sqrMagnitude;
			intersection = linePoint1 + (lineVec1 * s);
			return true;
		}
		else
		{
			intersection = Vector3.zero;
			return false;
		}
	}

	

	public static void ActivateScene(int a)
	{
		//Debug.Log("ActivateScene " + a + " sceneCount: " + SceneManager.sceneCount);
		//if (a >= SceneManager.sceneCount)
		//    return;
		//if (SceneManager.GetSceneAt(a) == null) return;

		if (!SceneManager.GetSceneByBuildIndex(a).isLoaded) return;
		//if (!SceneManager.GetSceneAt(a).isLoaded) return;

		//SceneManager.SetActiveScene(SceneManager.GetSceneAt(a));
		SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(a));

		//for (int i = 0; i < SceneManager.sceneCount; i++)
		for (int i = 1; i <= 2; i++)
		{
			//Scene s = SceneManager.GetSceneAt(i);
			Scene s = SceneManager.GetSceneByBuildIndex(i);

			if (a == i)
			{
				foreach (GameObject g in s.GetRootGameObjects()) g.SetActive(true);
			}
			else
			{
				//foreach (GameObject g in s.GetRootGameObjects()) g.SetActive(g == Game.Instance.gameObject);
			}
		}


	}

	public static string RomanNumber(int a)
	{
		switch (a)
		{
			case (1): return "I";
			case (2): return "II";
			case (3): return "III";
			case (4): return "IV";
			case (5): return "V";
			case (6): return "VI";
			case (7): return "VII";
			case (8): return "VIII";
			case (9): return "IX";
			case (10): return "X";
		}
		return a.ToString();
	}

	public static int[] Permutation(int l)
	{
		int[] o = new int[l];
		for (int i = 0; i < l; i++) o[i] = i;
		for (int i = 0; i < l; i++)
		{
			int k = Random.Range(0, i + 1);
			int value = o[k];
			o[k] = o[i];
			o[i] = value;
		}
		return o;
	}

	public static void Shuffle<T>(List<T> list)
	{
		int n = list.Count;
		while (n > 1)
		{
			n--;
			int k = Random.Range(0, n + 1);
			T value = list[k];
			list[k] = list[n];
			list[n] = value;
		}
	}

	public static Color RandomBrightColor()
	{
		return Color.HSVToRGB(Random.Range(0, 1f), 1, 1);
	}

	private void Update()
	{
		HandleTimeouts();
	}
}


