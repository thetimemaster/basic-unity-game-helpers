﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class UIStack
{
	static UIStack __default;
	public static UIStack Default
	{
		get
		{
			if (__default == null) __default = new UIStack();
			return __default;
		}
	}

	#region Events
	public abstract class StackEvent
	{
		public abstract void Back();
	}

	public class OpenUIEvent : StackEvent
	{
		AnimateUI opened;

		public OpenUIEvent(AnimateUI opened)
		{
			this.opened = opened;
		}

		public override void Back()
		{
			opened.CloseUI();
		}

		public override string ToString()
		{
			return "OpenUI " + opened.name;
		}
	}

	public class CloseUIEvent : StackEvent
	{
		AnimateUI closed;

		public CloseUIEvent(AnimateUI closed)
		{
			this.closed = closed;
		}

		public override void Back()
		{
			closed.OpenUI();
		}

		public override string ToString()
		{
			return "CloseUI " + closed.name;
		}
	}

	public class CustomEvent : StackEvent
	{
		System.Action onBack;

		public CustomEvent(System.Action onBack)
		{
			this.onBack = onBack;
		}

		public override void Back()
		{
			onBack.Invoke();
		}

		public override string ToString()
		{
			return "Important custom function";
		}
	}
	#endregion

	class StackFrame
	{
		internal AnimateUI caller;
		List<StackEvent> events;

		public StackFrame(AnimateUI caller)
		{
			this.caller = caller;
			events = new List<StackEvent>();
		}

		public void Back()
		{
			for (int i = events.Count - 1; i >= 0; i--)
			{
				events[i].Back();
			}
		}

		public void AddEvent(StackEvent @event)
		{
			// Debug.Log("Adding to Stack " + @event.ToString());
			events.Add(@event);
		}
	}

	StackFrame topFrame = null;
	Stack<StackFrame> frames;
	

	public UIStack()
	{
		frames = new Stack<StackFrame>();
	}

	public bool GoBack(AnimateUI caller)
	{

		// Debug.Log("Try stack back - "+caller.name);
		if (frames.Count > 0)
		{
			StackFrame frame = frames.Peek();
			if (frame.caller == caller)
			{
				frames.Pop();
				// Debug.Log("ok");
				frame.Back();
			}
			
			return true;
		}
		else return false;
	}

    public void StartNewFrame(AnimateUI caller)
	{
		// Debug.Log("New stack frame");
		topFrame = new StackFrame(caller);
		frames.Push(topFrame);
	}

	public void EndFrame()
	{
		// Debug.Log("End frame");
		topFrame = null;
	}

	public void ClearStack()
	{
		EndFrame();
		frames.Clear();
	}

	public void RegisterEvent(StackEvent @event)
	{
		if(topFrame != null)
		{
			topFrame.AddEvent(@event);
		}
	}
}
