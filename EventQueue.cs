﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventQueue
{
	public class Event
	{
		EventQueue parent;
		System.Action<Event> action;
		bool hasFinished;

		internal Event(System.Action<Event> action, EventQueue parent)
		{
			this.action = action;
			this.parent = parent;
			hasFinished = false;
		}

		public void Begin()
		{
			action.Invoke(this);
		}

		public void End()
		{
			if (hasFinished) return;

			hasFinished = true;
			parent.Dequeue();
		}
	}

	Queue<Event> events;
	bool eventRunning;

	public EventQueue()
	{
		events = new Queue<Event>();
		eventRunning = false;
	}

	public Event Enqueue(System.Action<Event> action)
	{
		Event e = new Event(action, this);

		events.Enqueue(e);

		if (!eventRunning && events.Count > 0) RunNext();

		return e;
	}

	internal void Dequeue()
	{
		events.Dequeue();
		eventRunning = false;

		if (events.Count > 0) RunNext();
	}

	private void RunNext()
	{
		eventRunning = true;
		events.Peek().Begin();
	}
}
