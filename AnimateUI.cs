﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;

public class AnimateUI : MonoBehaviour
{
	public static Dictionary<string, AnimateUI> openSoles = new Dictionary<string, AnimateUI>();

	public string soleCategory = "";

	public bool allowBack = true;
	public bool startOpen = false;
	public bool isOpen { get; private set; }

	[Header("Those AnimateUIs are recursively opened/closed.")]
	public List<AnimateUI> childUIs;

	Color defaultColor;
	Graphic graphic;
	RectTransform rt;
	Vector2 offsetMin, offsetMax;
	public enum AnimType { Scale, ScaleX, ScaleY, HideLeft, HideRight, HideTop, HideBottom, AlphaFade, None }
	public static float animationTime = 0.3f;
	public float animationTimeMultipler = 1;
	public AnimType animationType;
	AnimateFloat anim;
	float lastAnim = -2137;

	EventQueue.Event openedByEvent = null;

	[SerializeField]
	[ShowIf("ShowAlpha")]
	public float alphaFadeAlpha = -1;

	bool ShowAlpha => animationType == AnimType.AlphaFade;

	protected UIStack AssignedStack => UIStack.Default;


	static int bullshit = 0;

	bool isInitialized = false;

	public void SetOpen(bool newIsOpen)
	{
		if (isOpen && !newIsOpen) CloseUI();
		if (!isOpen && newIsOpen) OpenUI();
	}

	protected virtual void OnBack()
	{
		StackBack();
	}

	public AnimateUI Parent => transform?.parent.GetComponent<AnimateUI>();

	public virtual void Update()
	{
		isInitialized = true;

		if (allowBack && Input.GetKeyDown(KeyCode.Escape) && isOpen)
		{
			OnBack();
		}

		if (anim == null)
		{
			anim = new AnimateFloat(startOpen ? 1 : 0, animationTime * animationTimeMultipler);
			graphic = GetComponent<Graphic>();
			if (graphic)
			{
				defaultColor = graphic.color;
				if (alphaFadeAlpha != -1) defaultColor.a = alphaFadeAlpha;
			}
			isOpen = startOpen;
			rt = GetComponent<RectTransform>();

			if (isOpen && soleCategory != "")
			{
				int I = 0;
				while (I < 10 && openSoles.ContainsKey(soleCategory))
				{
					openSoles[soleCategory].CloseUI();
					I++;
					if (I == 10)
					{
						Debug.LogError("Probable CloseUI loop.");
						Debug.Break();
					}
				}

				openSoles.Add(soleCategory, this);
			}
		}

		float newAnim = anim;
		if (anim == lastAnim) return;
		lastAnim = newAnim;

		rt.offsetMin -= offsetMin;
		rt.offsetMax -= offsetMax;
		switch (animationType)
		{
			case (AnimType.Scale):
				transform.localScale = Vector3.one * newAnim;
				break;
			case (AnimType.AlphaFade):
				if (graphic != null)
				{
					graphic.color = new Color(defaultColor.r, defaultColor.g, defaultColor.b, defaultColor.a * newAnim);
					graphic.raycastTarget = newAnim > 0;
				}
				break;
			case (AnimType.ScaleY):
				transform.localScale = new Vector3(1, newAnim, 1);
				break;
			case (AnimType.ScaleX):
				transform.localScale = new Vector3(newAnim, 1, 1);
				break;
			case (AnimType.HideLeft):
				offsetMin = offsetMax = new Vector2(-rt.rect.width * (1 - lastAnim), 0);
				break;
			case (AnimType.HideRight):
				offsetMin = offsetMax = new Vector2(rt.rect.width * (1 - lastAnim), 0);
				break;
			case (AnimType.HideTop):
				offsetMin = offsetMax = new Vector2(0, rt.rect.height * (1 - lastAnim));
				break;
			case (AnimType.HideBottom):
				offsetMin = offsetMax = new Vector2(0, -rt.rect.height * (1 - lastAnim));
				break;
			default:
				break;
		}
		rt.offsetMin += offsetMin;
		rt.offsetMax += offsetMax;

		if (lastAnim == 0 && (animationType != AnimType.AlphaFade && animationType != AnimType.None)) gameObject.SetActive(false);
	}

	public virtual void OpenAsQueueEvent(EventQueue eventQueue)
	{
		eventQueue.Enqueue(ev =>
		{
			OpenUI();
			openedByEvent = ev;
		});
	}

	public static void CloseSole(string soleCategory)
	{
		if (soleCategory == "") return;

		int I = 0;
		while (I < 10 && openSoles.ContainsKey(soleCategory))
		{
			openSoles[soleCategory].CloseUI();
			I++;
			if (I == 10)
			{
				Debug.LogError("Probable CloseUI loop.");
				Debug.Break();
			}
		}
	}

	public virtual void OpenInStack()
	{
		AssignedStack.StartNewFrame(this);
		OpenUI();
		AssignedStack.EndFrame();
	}

	public virtual void StackBack()
	{
		AssignedStack.GoBack(this);
	}

	public virtual void OpenUI()
	{
		if (!isInitialized) Update();

		if (isOpen) return;
		isOpen = true;

		openedByEvent = null;

		if (soleCategory != "")
		{
			CloseSole(soleCategory);
			openSoles.Add(soleCategory, this);
		}

		if (gameObject.activeSelf == false) gameObject.SetActive(true);
		if (anim == null)
		{
			anim = new AnimateFloat(0, animationTime * animationTimeMultipler);
			graphic = GetComponent<Graphic>();
			if (graphic) defaultColor = graphic.color;
			rt = GetComponent<RectTransform>();
		}

		RegisterOpenEvent();
		anim.Set(1);

		if (childUIs != null)
			foreach (AnimateUI ui in childUIs) ui.OpenUI();
	}

	protected virtual void RegisterOpenEvent()
	{
		AssignedStack.RegisterEvent(new UIStack.OpenUIEvent(this));
			foreach (AnimateUI ui in childUIs) ui.OpenUI();
	}

	public virtual void CloseUI()
	{
		if (!isInitialized) Update();

		if (!isOpen) return;
		isOpen = false;

		if (soleCategory != "")
		{
			openSoles.Remove(soleCategory);
		}

		if (anim == null)
		{
			anim = new AnimateFloat(1, animationTime * animationTimeMultipler);
			graphic = GetComponent<Graphic>();
			if (graphic) defaultColor = graphic.color;
			rt = GetComponent<RectTransform>();
		}

		RegisterCloseEvent();
		anim.Set(0);

		if (childUIs != null)
		{
			foreach (AnimateUI ui in childUIs)
				ui.CloseUI();
		}

		if(openedByEvent != null)
		{
			Debug.Log("Closed event popup");
			Utils.SetTimeout(new Utils.Fun(() =>
			{
				openedByEvent.End();
			}), animationTime);
		}
	}

	protected virtual void RegisterCloseEvent()
	{
		AssignedStack.RegisterEvent(new UIStack.CloseUIEvent(this));
	}

	public virtual void ToggleUI()
	{
		if (isOpen) CloseUI();
		else OpenUI();
	}
}
