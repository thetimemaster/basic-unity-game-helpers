﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Text;

public class SaveHandler : MonoBehaviour
{
	static Dictionary<string, int> ints;
	static Dictionary<string, double> doubles;
	static Dictionary<string, float> floats;
	static Dictionary<string, string> strings;
	static bool stateChanged = false;
	static float changedTime = 0;
	static bool warnWhenUnsaved = false;

	static void Changed(bool quicksave=false)
	{
		stateChanged = true;
		if (quicksave) changedTime = -1;
	}

	public static string SaveFileString(string savename="")
	{
		return Application.persistentDataPath + "/save.txt";
	}

	public static void EraseSave()
	{
		if (File.Exists(SaveFileString()))
		{
			File.Delete(SaveFileString());
		}
		ints.Clear();
		doubles.Clear();
		strings.Clear();
		floats.Clear();
	}

	public static void SaveState()
	{
        changedTime = Time.realtimeSinceStartup;
		File.Delete(SaveFileString());
		FileStream save = File.OpenWrite(SaveFileString());
		StringBuilder sb = new StringBuilder("SaveVersion: 1\n");
		bool first = true;
		foreach(KeyValuePair<string,int> a in ints)
		{
			if (!first) sb.Append(";");
			sb.AppendFormat("{0}:{1};",a.Key,a.Value);
		}
		sb.Append("|");
		first = true;
		foreach (KeyValuePair<string, double> a in doubles)
		{
			if (!first) sb.Append(";");
			sb.AppendFormat("{0}:{1};", a.Key, a.Value);
		}
		sb.Append("|");
		first = true;
		foreach (KeyValuePair<string, float> a in floats)
		{
			if (!first) sb.Append(";");
			sb.AppendFormat("{0}:{1};", a.Key, a.Value);
		}
		sb.Append("|");
		first = true;
		foreach (KeyValuePair<string, string> a in strings)
		{
			if (!first) sb.Append(";");
			sb.AppendFormat("{0}:{1};", a.Key, a.Value);
		}
		sb.Append("|");
		byte[] b = Encoding.UTF8.GetBytes(sb.ToString());
		save.Write(b, 0, b.Length);
		save.Close();
	}

	public static void GenerateState()
	{
	}

	public static void LoadState()
	{
		ints = new Dictionary<string, int>();
		doubles = new Dictionary<string, double>();
		floats = new Dictionary<string, float>();
		strings = new Dictionary<string, string>();
		string[] lines = null;
		if (File.Exists(SaveFileString()))
		{
			lines = File.ReadAllLines(SaveFileString());
		}
		if(lines!=null)
		{ 
			if (lines.Length>0&&lines[0] == "SaveVersion: 1")
			{
				string input = lines[1];
				string[] x = input.Split('|');
				if (x.Length < 4)GenerateState();
				else
				{
					string[] intDef = x[0].Split(';');
					string[] doubleDef = x[1].Split(';');
					string[] floatDef = x[2].Split(';');
					string[] stringDef = x[3].Split(';');
					foreach (string s in intDef)
					{
						if (s.Length == 0) continue;
						string[] div = s.Split(new char[] { ':' }, 2);
						ints.Add(div[0], int.Parse(div[1]));
					}
					foreach (string s in doubleDef)
					{
						if (s.Length == 0) continue;
						string[] div = s.Split(new char[] { ':' }, 2);
						doubles.Add(div[0], double.Parse(div[1]));
					}
					foreach (string s in floatDef)
					{
						if (s.Length == 0) continue;
						string[] div = s.Split(new char[] { ':' }, 2);
						floats.Add(div[0], float.Parse(div[1]));
					}
					foreach (string s in stringDef)
					{
						if (s.Length == 0) continue;
						string[] div = s.Split(new char[] { ':' }, 2);
						strings.Add(div[0], div[1]);
					}
				}
			}
			else GenerateState();
		}
		else GenerateState();
	}

	void Awake()
	{
		Debug.Log(SaveFileString());
		LoadState();
	}

	public static void SetString(string name, string value,bool quicksave=false)
	{
		strings[name] = value;
		Changed(quicksave);
	}
	public static void SetFloat(string name, float value,bool quicksave=false)
	{
		floats[name] = value;
		Changed(quicksave);
	}
	public static void SetInt(string name, int value, bool quicksave = false)
	{
		ints[name] = value;
		Changed(quicksave);
	}
	public static void SetDouble(string name, double value, bool quicksave = false)
	{
		doubles[name] = value;
		Changed(quicksave);
	}

	public static string GetSavedString(string name,string unsavedValue="")
	{
		if(strings.ContainsKey(name))return strings[name];
		if(warnWhenUnsaved)Logger.LogWarning("Tried to load string '" + name + "' which was not saved.");
		return unsavedValue;
	}

	public static int GetSavedInt(string name,int unsavedValue=0)
	{
		if(ints.ContainsKey(name))return ints[name];
		if (warnWhenUnsaved) Logger.LogWarning("Tried to load int '" + name + "' which was not saved.");
		return unsavedValue;
	}

	public static double GetSavedDouble(string name,double unsavedValue=0)
	{
		if(doubles.ContainsKey(name))return doubles[name];
		if (warnWhenUnsaved) Logger.LogWarning("Tried to load double '" + name + "' which was not saved.");
		return unsavedValue;
	}

	public static float GetSavedFloat(string name, float unsavedValue = 0)
	{
		if(floats.ContainsKey(name))return floats[name];
		if (warnWhenUnsaved) Logger.LogWarning("Tried to load float '" + name + "' which was not saved.");
		return unsavedValue;
	}

	void Update()
	{
		if (stateChanged && Time.realtimeSinceStartup - changedTime > 2) SaveState();
	}
}
