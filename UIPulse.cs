﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIPulse : MonoBehaviour
{

	public bool shallPulse;
	bool inPulse;
	public float period,interval;
	public float minScale, maxScale;
	public Vector3 offsetWhenMin, offsetWhenMax;
	public bool defaultMin;
	float currScale;

	float time,toNext;

	private void Awake()
	{
		currScale = transform.localScale.x;
	}

	Vector3 OffsetForNow()
	{
		if (defaultMin) return offsetWhenMax + (Mathf.Cos(time / period * Mathf.PI * 2)) / 2 * (offsetWhenMax - offsetWhenMin);
		else return offsetWhenMin + (1 - Mathf.Cos(time / period * Mathf.PI * 2)) / 2 * (offsetWhenMax - offsetWhenMin);
	}

	private void Update()
	{
		transform.position -= OffsetForNow();

		if (shallPulse) inPulse = true;
		if (inPulse)
		{
			if (toNext < 0)
			{
				time += Time.deltaTime;
				if (time > period)
				{
					time=0;
					toNext = interval;
					if (!shallPulse)
					{
						inPulse = false;
					}
				}
				if(defaultMin)currScale = minScale + (Mathf.Cos(time / period * Mathf.PI * 2)) / 2 * (maxScale - minScale);
				else currScale = minScale + (1 - Mathf.Cos(time / period * Mathf.PI * 2)) / 2 * (maxScale - minScale);
			}
			else
			{
				toNext -= Time.deltaTime;
			}
		}
		transform.localScale = Vector3.one * currScale;

		transform.position += OffsetForNow();
	}

}
