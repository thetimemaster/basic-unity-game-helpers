﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[ExecuteAlways]
[RequireComponent(typeof(AspectRatioFitter))]
public class DynamicAspect : MonoBehaviour
{
	public float defaultAspect, xAspect, tabletAspect;

    void Update()
    {
		if (defaultAspect == 0) return;

		if (Utils.AspectRatio() > 0.7) GetComponent<AspectRatioFitter>().aspectRatio = tabletAspect;
		else if(Utils.AspectRatio() < 0.51) GetComponent<AspectRatioFitter>().aspectRatio = xAspect;
		else GetComponent<AspectRatioFitter>().aspectRatio = defaultAspect;
	}
}
